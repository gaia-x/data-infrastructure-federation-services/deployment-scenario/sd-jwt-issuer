package eu.gaiax.sdjwt.issuer.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto.DidService;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto.KeyService;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.DidDetails;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.KeyDetails;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.KeyImportDetails;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.DidRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.KeyImportRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.KeyRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response.KeyResponse;
import id.walt.crypto.keys.KeyManager;
import id.walt.crypto.keys.KeyType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@Tag(
        name = "Crypto API Management"
)
@RestController
@RequestMapping()
@RequiredArgsConstructor
@Slf4j
public class CryptoController {

    private final KeyService keyService;
    private final DidService didService;
    private final ObjectMapper objectMapper;


    @Operation(
            summary = "Create a new key pair for use",
            description = "Create a new key pair for use" +
                    "\n\n Supported keyTypes: *RSA, Ed25519, secp256k1*" +
                    "\n\n For RSA, request MUST contain keySize which is at least 2048 bits"
    )
    @PostMapping("/crypto/keys")
    public ResponseEntity<?> createKeyPair(@RequestBody @Valid KeyRequest keyRequest){
        KeyDetails keyDetails = keyService.create(keyRequest);
        log.error("Key material: {}", keyDetails.getKeyMaterial());
        KeyResponse response = new KeyResponse(keyDetails.getKeyId(),
                keyDetails.getKeyType(),
                KeyManager.INSTANCE.resolveSerializedKey(keyDetails.getKeyMaterial()).getPublicKeyBlocking().exportJWKBlocking());

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Import a Keypair for use",
            description = "Import a Keypair for use" +
                    "\n\n Supported keyTypes: *RSA, Ed25519, secp256k1*"
    )
    @PostMapping("/crypto/keys/import")
    public ResponseEntity<?> importKey(@RequestBody @Valid KeyImportRequest request){
        KeyImportDetails importDetails = new KeyImportDetails(request.getKeyType(),
                Map.of("type", "jwk", "jwk", request.getJwk()));

        KeyDetails keyDetails = keyService.importKey(importDetails);
        KeyResponse response = new KeyResponse(keyDetails.getKeyId(),
                keyDetails.getKeyType(),
                KeyManager.INSTANCE.resolveSerializedKey(keyDetails.getKeyMaterial()).getPublicKeyBlocking().exportJWKBlocking());

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


    @Operation(
            summary = "Delete a key pair by given keyId",
            description = "Delete a key pair by given keyId"
    )
    @DeleteMapping("/crypto/keys/{keyId}")
    public ResponseEntity<?> deleteKeyById(@PathVariable(name = "keyId") String keyId){
        keyService.deleteKeyById(Long.parseLong(keyId));

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(
            summary = "Get public key details by given keyId",
            description = "Get public key details by given keyId"
    )
    @GetMapping("/crypto/keys/{keyId}")
    public ResponseEntity<?> getKeyById(@PathVariable(name = "keyId") String keyId){
        KeyDetails keyDetails = keyService.getKeyDetailsById(keyId);
        KeyResponse response = new KeyResponse(keyDetails.getKeyId(),
                keyDetails.getKeyType(),
                KeyManager.INSTANCE.resolveSerializedKey(keyDetails.getKeyMaterial()).getPublicKeyBlocking().exportJWKBlocking());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(
            summary = "Get all generated key details",
            description = "Get all generated key details"
    )
    @GetMapping("/crypto/keys")
    public ResponseEntity<?> getAllKeys(){
        List<KeyDetails> keys = keyService.getAllKeys();
        List<KeyResponse> responses = keys.stream().map(key ->
                        new KeyResponse(key.getKeyId(),
                                key.getKeyType(),
                                KeyManager.INSTANCE.resolveSerializedKey(key.getKeyMaterial()).getPublicKeyBlocking().exportJWKBlocking()))
                .toList();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }


    @Operation(
            summary = "Create a new DID by keyId",
            description = "Create a new DID submitted keyId" +
                    "\n\n By default, the system concatenates a path, which is keyId, to the server baseUrl to construct a complete a did:web" +
                    "\n\n For example, with domain: *jwt-issuer.aster-x.demo23.gxfs.fr* and keyId: *1*, the system issues a complete web as followings: *did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1*"
    )
    @PostMapping("/crypto/dids")
    public ResponseEntity<?> createDid(@RequestBody @Valid DidRequest request){
        DidDetails didDetails = didService.createDid(request.getKeyId());
        return new ResponseEntity<>(didDetails, HttpStatus.CREATED);
    }


    @Operation(
            summary = "Get a DID by path",
            description = "Get a DID by path"
    )
    @GetMapping("/{path}/did.json")
    public ResponseEntity<?> getDidByPath(@PathVariable(name = "path") String path){
        try {
            DidDetails didDetails = didService.getDidByPath(path);
            Map<String, Object> didDocument = objectMapper.readValue(didDetails.getDidDocument(), new TypeReference<Map<String, Object>>() {});
            return new ResponseEntity<>(didDocument, HttpStatus.OK);

        } catch (JsonProcessingException e) {
            log.error("Failed to get Did Document due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }



    @Operation(
            summary = "Get all generated Did Details",
            description = "Get all generated Did Details"
    )
    @GetMapping("/crypto/dids")
    public ResponseEntity<?> getAllDidDocuments(){
        List<DidDetails> didDetailsList = didService.getAllDidDetails();

        return new ResponseEntity<>(didDetailsList, HttpStatus.OK);
    }
}