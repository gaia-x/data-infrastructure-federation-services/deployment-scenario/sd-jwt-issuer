package eu.gaiax.sdjwt.issuer.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.enums.ProofTypes;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.LdSignatureRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.LdVerificationRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response.LdSignatureResponse;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response.LdVerificationResponse;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.signer.LdSignerService;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.verifier.LdVerifierService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.Map;

@Tag(
        name = "Json-ld Credential Signing Service"
)
@RestController
@RequestMapping(
        path = "/credentials"
)
@Slf4j
@RequiredArgsConstructor
public class LdSigningController {

    private final LdSignerService ldSignerService;
    private final LdVerifierService ldVerifierService;

    private final ObjectMapper objectMapper;


    @Operation(
            summary = "Signing a credential with multiple signers",
            description = "Signing a credential by submitting Proof Config as well as KeyID and CryptoSuites as below: " +
                    "\n\n **RsaSignature2018** supports RSA" +
                    "\n\n **JsonWebSignature2020** supports RSA, Ed25519, secp256k1" +
                    "\n\n **Ed25519Signature2020, Ed25519Signature2018** supports Ed25519" +
                    "\n\n **EcdsaSecp256k1Signature2019** supports secp256k1"
    )
    @PostMapping("/sign")
    public ResponseEntity<?> sign(@RequestParam(name = "proofType", defaultValue = "PROOF_SET") ProofTypes proofType,
                                  @RequestBody @Valid LdSignatureRequest signingRequest){
        try {
            log.info("Received a signing request for credential: {}", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(signingRequest.getVc()));
            Map<String, Object> vc = signingRequest.getVc();
            DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
            vc.put("issuanceDate", formatter.format(Instant.now().truncatedTo(ChronoUnit.SECONDS)));
            vc.put("expirationDate", formatter.format(Instant.now().plus(30, ChronoUnit.DAYS).truncatedTo(ChronoUnit.SECONDS)));
            signingRequest.setVc(vc);
            LdSignatureResponse response =  ldSignerService.sign(signingRequest);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (JsonProcessingException e) {
            log.error("Failed to process the credential due to: {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }


    @Operation(
            summary = "Verifying a credential signature created by multiple signers",
            description = "Verifying a credential signature created by multiple signers"
    )
    @PostMapping("/verify")
    public ResponseEntity<?> verify(@RequestBody @Valid LdVerificationRequest request){
        LdVerificationResponse response = ldVerifierService.verify(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
