package eu.gaiax.sdjwt.issuer.controller;


import eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.request.JwtBaseIssuanceRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.request.SdJwtBaseIssuanceRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.response.CredentialResponse;
import eu.gaiax.sdjwt.issuer.service.issuer.jwt.IssuerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@Tag(
        name = "Jwt & Sd-jwt Issuing Service",
        description = "Service for issuing Jwt Verifiable Credentials"
)
@RestController
@Slf4j
@RequiredArgsConstructor
public class JwtIssuerController {

    private final IssuerService issuerService;

    @Operation(
            summary = "Issuing SD-JWT verifiable credentials",
            description = "Issuing sd-jwt verifiable credentials by providing keys, credential subject data, and attributes to be hidden. By default, the issuing service has already a default and configurable DID and cryptographic Key pair" +
                    "\n\n in case that a new Key and DID provided in the request payload, they will replace the default one"
    )
    @PostMapping("/issue/sd-jwt")
    public ResponseEntity<?> issueSdJwtCredential(@Valid  @RequestBody SdJwtBaseIssuanceRequest sdJwtBaseIssuanceRequest){
        log.info("Processing a new Sd-Jwt Credential Request for {}", sdJwtBaseIssuanceRequest.getSubjectDid());

        SdJwtBaseIssuanceRequest.SdJwtIssuanceRequest sdJwtIssuanceRequest = sdJwtBaseIssuanceRequest.toSdJwtIssuanceRequest();
        CredentialResponse issuedVC = issuerService.generateSdJwtCredential(sdJwtIssuanceRequest);

        return new ResponseEntity<>(issuedVC, HttpStatus.OK);
    }


    @Operation(
            summary = "Issuing VC-JWT verifiable credentials",
            description = "Issuing vc-jwt verifiable credentials by providing keys, credential subject data, and attributes to be hidden. By default, the issuing service has already a default and configurable DID and cryptographic Key pair" +
                    "\n\n in case that a new Key and DID provided in the request payload, they will replace the default one"
    )
    @PostMapping("/issue/jwt")
    public ResponseEntity<?> issueJwtCredential(@Valid @RequestBody JwtBaseIssuanceRequest baseJwtIssuanceRequest){
        log.info("Processing a new Jwt Credential Request for {}", baseJwtIssuanceRequest.getSubjectDid());

        JwtBaseIssuanceRequest.JwtIssuanceRequest jwtIssuanceRequest = baseJwtIssuanceRequest.toJwtIssuanceRequest();

        CredentialResponse issuedVC = issuerService.generateJwtCredential(jwtIssuanceRequest);
        return new ResponseEntity<>(issuedVC, HttpStatus.OK);
    }
}