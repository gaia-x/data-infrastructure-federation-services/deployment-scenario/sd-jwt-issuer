package eu.gaiax.sdjwt.issuer.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import id.walt.credentials.vc.vcs.W3CVC;
import kotlinx.serialization.json.Json;
import kotlinx.serialization.json.JsonElement;
import kotlinx.serialization.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;


@Slf4j
public class IssuanceUtils {

    private final static Gson gson = new Gson();
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static W3CVC convertToW3CVC(Map<String, Object> requestData){
        Map<String, JsonElement> data = new HashMap<>();
        requestData.forEach((key, value) -> {
            JsonElement jsonElement = convertObjectToJSONElement(value);
            data.put(key, jsonElement);
        });

        return new W3CVC(data);
    }

    public static String getIssuerFromCredentialTemplate(Map<String, Object> requestData){
        ObjectNode objectNode = objectMapper.valueToTree(requestData);
        JsonNode issuer = objectNode.get("issuer").get("id");
        if(issuer != null && !StringUtils.isEmpty(issuer.asText()))
            return issuer.asText();
        else
            return null;
    }

    private static JsonElement convertObjectToJSONElement(Object value){
        String body = gson.toJson(value);
        return Json.Default.parseToJsonElement(body);
    }
}
