package eu.gaiax.sdjwt.issuer.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import eu.gaiax.sdjwt.issuer.service.constants.OpenId4VCCredentialConstants;
import id.walt.credentials.vc.vcs.W3CVC;
import kotlinx.serialization.json.Json;
import kotlinx.serialization.json.JsonElement;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class CredentialUtils {

    private static final Gson gson = new Gson();

    public static W3CVC toW3CVC(Map<String, Object> requestData){

        Map<String, JsonElement> data = new HashMap<>();
        requestData.forEach((key, value) -> {
            JsonElement jsonElement = convertObjectToJSONElement(value);
            data.put(key, jsonElement);
        });

        return new W3CVC(data);
    }

    private static JsonElement convertObjectToJSONElement(Object value){
        String body = gson.toJson(value);
        return Json.Default.parseToJsonElement(body);
    }

    public static Map<String, Object> toVCMap(W3CVC w3CVC){
        Map<String, Object> vc = new HashMap<>();

        w3CVC.getKeys().forEach( key -> {
            vc.put(key, w3CVC.get(key));
        });
        return vc;
    }

    public static W3CVC updateSubjectAndIssuerDids(W3CVC vc, String subjectDid, String issuerDid){
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> vcMap = objectMapper.readValue(vc.toJson(), new TypeReference<Map<String, Object>>() {});

            // Update Subject DID
            Map<String, Object> credentialSubject = (Map<String, Object>) vcMap.get(OpenId4VCCredentialConstants.W3CDM2_CREDENTIAL_SUBJECT);
            credentialSubject.put(OpenId4VCCredentialConstants.W3CDM2_CREDENTIAL_SUBJECT_ID, subjectDid);
            vcMap.put(OpenId4VCCredentialConstants.W3CDM2_CREDENTIAL_SUBJECT, credentialSubject);

            // Update Issuer DID
            Map<String, Object> issuer = (Map<String, Object>) vcMap.get(OpenId4VCCredentialConstants.W3CDM2_ISSUER);
            issuer.put(OpenId4VCCredentialConstants.W3CDM2_ISSUER_ID, issuerDid);
            vcMap.put(OpenId4VCCredentialConstants.W3CDM2_ISSUER, issuer);


            W3CVC updatedVC =  toW3CVC(vcMap);
            log.info("Credential with updated DID: {}", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(updatedVC));

            return updatedVC;
        } catch (JsonProcessingException e) {
            log.error("Failed to update did subject due to: {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }

    }

    public static W3CVC populateAttributes(W3CVC vc, Map<String, String> attributeMaps){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> vcMap = objectMapper.readValue(vc.toJson(), new TypeReference<Map<String, Object>>() {});
            attributeMaps.keySet().forEach(key -> vcMap.put(key, attributeMaps.get(key)));

            W3CVC updatedVC =  toW3CVC(vcMap);
            log.info("Final Credential Object: {}", updatedVC.toJson());

            return updatedVC;

        } catch (JsonProcessingException e) {
            log.error("Could not populate attributes into token due to: {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }
}
