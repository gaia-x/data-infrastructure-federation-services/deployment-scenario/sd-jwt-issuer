package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LdVerificationResponse {

    private boolean overall;
    private List<VerificationElement> verificationElements = new ArrayList<>();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class VerificationElement {
        private String keyId;
        private boolean result;
    }
}