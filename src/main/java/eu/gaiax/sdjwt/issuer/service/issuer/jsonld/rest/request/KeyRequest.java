package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request;

import id.walt.crypto.keys.KeyType;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyRequest {

    @NotNull
    private KeyType keyType;
    private KeyMetadata metadata;


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class KeyMetadata {
        private String keySize;
    }
}