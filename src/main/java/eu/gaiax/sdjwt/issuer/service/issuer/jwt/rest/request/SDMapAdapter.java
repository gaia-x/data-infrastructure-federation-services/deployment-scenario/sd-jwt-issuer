package eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.request;


import id.walt.sdjwt.DecoyMode;
import id.walt.sdjwt.SDField;
import id.walt.sdjwt.SDMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class SDMapAdapter {
    private Map<String, SDFieldAdapter> fields;
    private DecoyMode decoyMode = DecoyMode.NONE;
    private int decoys;

    public SDMap convertToSDMap() {
        Map<String, SDField> convertedFields = new HashMap<>();
        for (Map.Entry<String, SDFieldAdapter> entry : fields.entrySet()) {
            convertedFields.put(entry.getKey(), convertField(entry.getValue()));
        }
        return new SDMap(convertedFields, decoyMode, decoys);
    }

    // Recursive method to handle nested SDFieldAdapters
    private SDField convertField(SDFieldAdapter fieldAdapter) {
        SDMap nestedSdMap = null;
        if (fieldAdapter.getChildren() != null) {
            nestedSdMap = fieldAdapter.getChildren().convertToSDMap();
        }

        return new SDField(fieldAdapter.getSd(), nestedSdMap);
    }

}