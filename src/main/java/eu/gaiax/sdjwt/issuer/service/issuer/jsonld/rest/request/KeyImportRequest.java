package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request;


import com.fasterxml.jackson.annotation.JsonIgnore;
import id.walt.crypto.keys.KeyType;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyImportRequest {

    @JsonIgnore
    private String type = "jwk";

    @NotNull
    private KeyType keyType;

    @NotEmpty
    private String jwk;
}
