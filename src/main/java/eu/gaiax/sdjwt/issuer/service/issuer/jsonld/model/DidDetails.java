package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DidDetails {
    private String id;
    private String did;
    private String didDocument;
}
