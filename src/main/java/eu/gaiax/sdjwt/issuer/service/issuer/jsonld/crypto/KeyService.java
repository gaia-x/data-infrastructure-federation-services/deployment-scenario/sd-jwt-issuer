package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.sdjwt.issuer.entity.KeyEntity;
import eu.gaiax.sdjwt.issuer.exception.ResourceNotFoundException;
import eu.gaiax.sdjwt.issuer.respository.KeyRepository;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.KeyImportDetails;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.KeyRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.KeyDetails;
import id.walt.crypto.keys.JwkKeyMeta;
import id.walt.crypto.keys.KeySerialization;
import id.walt.crypto.keys.KeyType;
import id.walt.crypto.keys.jwk.JWKKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class KeyService {


    private final KeyRepository keyRepository;
    private final ObjectMapper objectMapper;


    public KeyDetails create(KeyRequest keyRequest){
        log.info("Creating a new key for algo: {}",  keyRequest.getKeyType().name());


        JwkKeyMeta jwkKeyMeta = null;
        if(keyRequest.getKeyType() == KeyType.RSA){
            if(keyRequest.getMetadata() == null || StringUtils.isEmpty(keyRequest.getMetadata().getKeySize())){
                log.error("Received a RSA key creation without specifying keySize");
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Received a RSA key creation without specifying keySize");
            }
            jwkKeyMeta = new JwkKeyMeta(UUID.randomUUID().toString(), Integer.parseInt(keyRequest.getMetadata().getKeySize()));
        }
        JWKKey jwkKey = JWKKey.Companion.generateBlocking(keyRequest.getKeyType(), jwkKeyMeta);

        KeyEntity key = new KeyEntity();
        key.setKeyType(keyRequest.getKeyType());
        key.setJwk(KeySerialization.INSTANCE.serializeKey(jwkKey));
        key.setCreatedAt(LocalDateTime.now());
        log.info("Created key has type: {} and jwk: {}",key.getKeyType(), key.getJwk());
        KeyEntity createdKey = keyRepository.save(key);

        log.info("Successfully created key with type: {}", keyRequest.getKeyType());
        return new KeyDetails(
                String.valueOf(createdKey.getId()),
                createdKey.getKeyType(),
                createdKey.getJwk(),
                null,
                null);
    }

    public KeyDetails importKey(KeyImportDetails importDetails){
        try {
            String jwk = objectMapper.writeValueAsString(importDetails.getDetails());
            log.info("Importing a new key: {}", jwk);

//            JWKKey jwkKey= (JWK)KeyManager.INSTANCE.resolveSerializedKey(jwk);

            KeyEntity keyEntity = new KeyEntity();
            keyEntity.setKeyType(importDetails.getKeyType());
            keyEntity.setJwk(jwk);
            keyEntity.setCreatedAt(LocalDateTime.now());

            KeyEntity importedKey = keyRepository.save(keyEntity);
            log.info("Successfully imported key with type: {}", importDetails.getKeyType());

            return new KeyDetails(
                    String.valueOf(importedKey.getId()),
                    importedKey.getKeyType(),
                    jwk,
                    null,
                    null);
        } catch (JsonProcessingException e) {
            log.error("Failed to import a key with type {} due to {}", importDetails.getKeyType(), ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }

    }


    public KeyDetails getKeyDetailsById(String id){
        KeyEntity key = keyRepository.findById(Long.parseLong(id)).orElseThrow(() -> {
            log.error("Could not find key with id: {}", id);
            return new ResourceNotFoundException("Key", "ID", id);
        });
        String did = null;
        String didDocument = null;
        if(key.getDidEntity() != null) {
            did = key.getDidEntity().getDid();
            didDocument = key.getDidEntity().getDidDocument();
        }
        return new KeyDetails(
                String.valueOf(key.getId()),
                key.getKeyType(),
                key.getJwk(),
                did,
                didDocument);
    }


    public List<KeyDetails> getAllKeys(){
        List<KeyEntity> keys = keyRepository.findAll();
        return keys.stream().map( key -> {
            String did = null;
            String didDocument = null;
            if(key.getDidEntity() !=  null){
                did = key.getDidEntity().getDid();
                didDocument = key.getDidEntity().getDidDocument();
            }
            return new KeyDetails(String.valueOf(key.getId()),
                    key.getKeyType(),
                    key.getJwk(),
                    did,
                    didDocument);
        }).toList();
    }

    public void deleteKeyById(Long id){
        log.info("Deleting Key with id: {}", id);
        keyRepository.deleteById(id);
    }

}
