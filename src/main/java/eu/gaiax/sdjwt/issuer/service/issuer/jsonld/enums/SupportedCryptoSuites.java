package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.enums;


import id.walt.crypto.keys.KeyType;
import lombok.Getter;

import java.util.List;

@Getter
public enum SupportedCryptoSuites {

    EcdsaSecp256k1Signature2019("EcdsaSecp256k1Signature2019", List.of(KeyType.secp256k1.name())),
    Ed25519Signature2018("Ed25519Signature2018", List.of(KeyType.Ed25519.name())),
    Ed25519Signature2020("Ed25519Signature2020", List.of(KeyType.Ed25519.name())),
    JcsEcdsaSecp256k1Signature2019("JcsEcdsaSecp256k1Signature2019", List.of()),
    JcsEd25519Signature2020("JcsEd25519Signature2020", List.of(KeyType.secp256k1.name())),
    JsonWebSignature2020("JsonWebSignature2020", List.of(KeyType.RSA.name(), KeyType.Ed25519.name(), KeyType.secp256r1.name(), KeyType.secp256k1.name())),
    RsaSignature2018("RsaSignature2018", List.of(KeyType.RSA.name()));


    private final String details;
    private final List<String> supportedKeyTypes;

    SupportedCryptoSuites(String details, List<String> supportedKeyTypes){
        this.details = details;
        this.supportedKeyTypes = supportedKeyTypes;
    }

}
