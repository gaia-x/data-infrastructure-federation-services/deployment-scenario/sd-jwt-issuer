package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LdVerificationRequest {
    private Map<String, Object> vc;
}
