package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.signer;


import com.apicatalog.jsonld.loader.DocumentLoader;
import com.apicatalog.jsonld.loader.HttpLoader;
import com.danubetech.keyformats.crypto.impl.Ed25519_EdDSA_PrivateKeySigner;
import com.danubetech.keyformats.crypto.impl.RSA_RS256_PrivateKeySigner;
import com.danubetech.keyformats.crypto.impl.secp256k1_ES256K_PrivateKeySigner;
import com.danubetech.keyformats.crypto.impl.secp256k1_ES256K_PublicKeyVerifier;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import eu.gaiax.sdjwt.issuer.service.converter.KeyConverter;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.contexts.AsterXJsonLDContexts;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto.DidService;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.KeyDetails;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto.KeyService;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.enums.SupportedCryptoSuites;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.LdSignatureRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response.LdSignatureResponse;
import foundation.identity.jsonld.ConfigurableDocumentLoader;
import foundation.identity.jsonld.JsonLDException;
import foundation.identity.jsonld.JsonLDObject;
import id.walt.crypto.keys.KeyType;
import info.weboftrust.ldsignatures.LdProof;
import info.weboftrust.ldsignatures.signer.*;
import info.weboftrust.ldsignatures.verifier.JsonWebSignature2020LdVerifier;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class LdSignerService {

    private final KeyService keyService;
    private final ObjectMapper objectMapper;

    @Value("${server.base-url}")
    private String baseUrl;
    private DocumentLoader documentLoader;

    @PostConstruct
    public void initDocumentLoader(){
        ConfigurableDocumentLoader configurableDocumentLoader = new ConfigurableDocumentLoader(AsterXJsonLDContexts.CONTEXTS);
        configurableDocumentLoader.setHttpLoader(HttpLoader.defaultInstance());
        configurableDocumentLoader.setEnableHttp(true);
        configurableDocumentLoader.setEnableHttps(true);

        documentLoader =   configurableDocumentLoader;
    }

    public LdSignatureResponse sign(LdSignatureRequest signatureRequest){
        try {

            // validate key type and crypto suite
            log.info("Validating submitted keyTypes against cryptoSuites...");
            validateSigners(signatureRequest.getSigners());

            log.info("Done ! Continue signing process");
            String vc = objectMapper.writeValueAsString(signatureRequest.getVc());
            JsonLDObject vcTbs = JsonLDObject.fromJson(vc);
//            vcTbs.setDocumentLoader(AsterXJsonLDContexts.DOCUMENT_LOADER);
            vcTbs.setDocumentLoader(documentLoader);

            // Sort signers
            List<LdSignatureRequest.Signer> signers = signatureRequest.getSigners().stream()
                    .sorted(Comparator.comparingInt(LdSignatureRequest.Signer::getSigningOrder))
                    .toList();

            // extract key from database
            signers.forEach(signer -> {
                KeyDetails keyDetails = keyService.getKeyDetailsById(signer.getKeyId());
                //Initiate signer with corresponding crypto suite
                KeyType keyType = keyDetails.getKeyType();
                String jwk = keyDetails.getKeyMaterial();
                SupportedCryptoSuites cryptoSuite = signer.getCryptoSuite();

                LdSigner jsonldSigner = getLdSignerForCryptoSuiteAndKeyPair(cryptoSuite, keyType, jwk);
                jsonldSigner.setCreated(new Date());
                jsonldSigner.setProofPurpose("assertionMethod");
                jsonldSigner.setVerificationMethod(URI.create(getVerificationMethodForKeyId(keyDetails.getKeyId())));

                try {
                    LdProof ldProof = jsonldSigner.sign(vcTbs);
                    log.info("ld proof: {}", ldProof);
                } catch (IOException | GeneralSecurityException | JsonLDException e) {
                    log.error("Failed to sign credential due to {}", ExceptionUtils.getMessage(e));
                    throw new RuntimeException(e);
                }


            });

            LdSignatureResponse response = new LdSignatureResponse();
            response.setVc(vcTbs.toMap());
            return response;


        } catch (JsonProcessingException e) {
            log.error("Failed to read the submitted vc due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }


    private void validateSigners(List<LdSignatureRequest.Signer> signers){
        signers.forEach(signer -> {
            KeyDetails keyDetails = keyService.getKeyDetailsById(signer.getKeyId());
            if(! verifyKeyTypeAgainstCryptoSuites(keyDetails.getKeyType(), signer.getCryptoSuite())){
                log.error("Signing request contains a keyId: {}, having unmatched keyType: {} and cryptoSuite: {}", keyDetails.getKeyId(), keyDetails.getKeyType(), signer.getCryptoSuite());
                throw new IllegalArgumentException(String.format("Signing request contains a keyId: %s having an unmatched keyType: %s and cryptoSuite: %s", keyDetails.getKeyId(), keyDetails.getKeyType().name(), signer.getCryptoSuite().getDetails()));
            }
        });
    }

    private String getVerificationMethodForKeyId(String keyId){
        KeyDetails keyDetails = keyService.getKeyDetailsById(keyId);
        if(StringUtils.isEmpty(keyDetails.getDid())){
            return baseUrl + "/crypto/keys/" + keyDetails.getKeyId();
        }

        String didDocument = keyDetails.getDidDocument();
        try {
            JsonNode didNode = objectMapper.readTree(didDocument);
            ArrayNode assertionMethod = (ArrayNode) didNode.get("assertionMethod");
            String verificationMethod = assertionMethod.get(0).asText();

            log.info("Extracted verification method: {} for keyId: {}", verificationMethod, keyId);

            return verificationMethod;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean verifyKeyTypeAgainstCryptoSuites(KeyType keyType, SupportedCryptoSuites cryptoSuites){
        return cryptoSuites.getSupportedKeyTypes().contains(keyType.name());
    }


    private LdSigner getLdSignerForCryptoSuiteAndKeyPair(SupportedCryptoSuites cryptoSuites, KeyType keyType, String jwk){
        log.info("Creating a LD Signer for cryptosuite: {} and keyType: {}", cryptoSuites, keyType.name());

        return switch (cryptoSuites.getDetails()){
            case "RsaSignature2018" -> new RsaSignature2018LdSigner(KeyConverter.jsonToKey(keyType, jwk));
            case "JsonWebSignature2020" -> {
                if(keyType == KeyType.RSA)
                    yield new JsonWebSignature2020LdSigner(new RSA_RS256_PrivateKeySigner(KeyConverter.jsonToKey(keyType, jwk)));
                else if (keyType == KeyType.Ed25519)
                    yield new JsonWebSignature2020LdSigner(new Ed25519_EdDSA_PrivateKeySigner(KeyConverter.jsonToEd25519PrivateKey(jwk)));
                else if (keyType == KeyType.secp256k1){
                    org.bitcoinj.core.ECKey key = KeyConverter.jsonToBitcoinECKey(jwk);
                    yield new JsonWebSignature2020LdSigner(new secp256k1_ES256K_PrivateKeySigner(key));
                }
                else
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("the keyType: %s is not supported yet for JsonWebSignature2020", keyType.name()));
            }
            case "Ed25519Signature2018" -> new Ed25519Signature2018LdSigner(KeyConverter.jsonToEd25519PrivateKey(jwk));
            case "Ed25519Signature2020" -> new Ed25519Signature2020LdSigner(KeyConverter.jsonToEd25519PrivateKey(jwk));
            case "EcdsaSecp256k1Signature2019" -> {
                    org.bitcoinj.core.ECKey key = KeyConverter.jsonToBitcoinECKey(jwk);
                    yield (new EcdsaSecp256k1Signature2019LdSigner(new secp256k1_ES256K_PrivateKeySigner(key)));
            }
            default -> throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Cryptosuite: %s is not supported", cryptoSuites.getDetails()));
        };
    }

}
