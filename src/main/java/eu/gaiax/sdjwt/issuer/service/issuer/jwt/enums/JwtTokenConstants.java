package eu.gaiax.sdjwt.issuer.service.issuer.jwt.enums;

public class JwtTokenConstants {

    public static final String JWT_HEADER_TYPE = "typ";
    public static final String JWT_PAYLOAD_ISSUER = "iss";
    public static final String JWT_PAYLOAD_NOT_BEFORE = "nbf";

    public static final String JWT_PAYLOAD_TOKEN_ID = "jti";
    public static final String JWT_PAYLOAD_ISSUED_AT = "iat";
}
