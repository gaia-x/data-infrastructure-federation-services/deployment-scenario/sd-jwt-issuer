package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto;

import eu.gaiax.sdjwt.issuer.entity.DidEntity;
import eu.gaiax.sdjwt.issuer.entity.KeyEntity;
import eu.gaiax.sdjwt.issuer.exception.ResourceNotFoundException;
import eu.gaiax.sdjwt.issuer.respository.DidRepository;
import eu.gaiax.sdjwt.issuer.respository.KeyRepository;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.DidDetails;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.KeyDetails;
import id.walt.crypto.keys.KeyManager;
import id.walt.crypto.keys.jwk.JWKKey;
import id.walt.did.dids.registrar.DidResult;
import id.walt.did.dids.registrar.dids.DidWebCreateOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DidService {

    @Value("${server.base-url}")
    private String baseUrl;
    private final DidRepository didRepository;
    private final KeyRepository keyRepository;

    public DidDetails createDid(String keyId){
        KeyEntity keyEntity = keyRepository.findById(Long.parseLong(keyId)).orElseThrow(() -> {
            log.error("Could not find a key entity with keyId: {}", keyId);
            return new ResourceNotFoundException("Key", "keyId", keyId);
        });
        if(keyEntity.getDidEntity() != null){
            log.error("Provided Key with Id: {} already associated to a DID", keyId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Provided keyId with id: %s already associated to a DID", keyId));
        }
        JWKKey key = (JWKKey) KeyManager.INSTANCE.resolveSerializedKey(keyEntity.getJwk());

        DidResult didResult = id.walt.did.dids.DidService.INSTANCE.javaRegisterByKeyBlocking("web", key, new DidWebCreateOptions(baseUrl, keyId, keyEntity.getKeyType()));
        DidEntity didEntity = new DidEntity();
        didEntity.setCreatedAt(LocalDateTime.now());
        didEntity.setDidPath(keyId);
        didEntity.setDidDocument(didResult.getDidDocument().toString());
        didEntity.setDid(didResult.getDid());

        didEntity.setKeyEntity(keyEntity);
        keyEntity.setDidEntity(didEntity);

        DidEntity createdDid = didRepository.save(didEntity);

        return new DidDetails(String.valueOf(createdDid.getId()), createdDid.getDid(), createdDid.getDidDocument());
    }

    public KeyDetails getKeyDetailsFromDid(String did){
        DidEntity didEntity = didRepository.findByDid(did).orElseThrow(() -> {
            log.error("Could not find a did entity with given did: {}", did);
            return new ResourceNotFoundException("DID Entity", "DID", did);
        });

        KeyEntity key = didEntity.getKeyEntity();
        return new KeyDetails(String.valueOf(key.getId()),
                key.getKeyType(),
                key.getJwk(),
                didEntity.getDid(),
                didEntity.getDidDocument()
        );
    }

    public boolean existByPath(String path){
        return didRepository.existsByDidPath(path);
    }

    public DidDetails getDidByPath(String path){
        DidEntity didEntity = didRepository.findByDidPath(path).orElseThrow(() -> {
            log.error("Could not find a did with path: {}", path);
            return new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Could not find a DID with path: %s", path));
        });

        return new DidDetails(String.valueOf(didEntity.getId()), didEntity.getDid(), didEntity.getDidDocument());
    }

    public List<DidDetails> getAllDidDetails(){
        List<DidEntity> entities = didRepository.findAll();

        return entities.stream().map(did ->
                new DidDetails(String.valueOf(did.getId()), did.getDid(), did.getDidDocument()))
                .toList();
    }
}