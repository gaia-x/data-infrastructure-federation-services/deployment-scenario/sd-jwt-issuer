package eu.gaiax.sdjwt.issuer.service.issuer.jwt;


import eu.gaiax.sdjwt.issuer.service.issuer.jwt.enums.JwtTokenConstants;
import eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.request.JwtBaseIssuanceRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.request.SdJwtBaseIssuanceRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.response.CredentialResponse;
import eu.gaiax.sdjwt.issuer.utils.CredentialUtils;
import id.walt.credentials.vc.vcs.W3CVC;
import id.walt.crypto.keys.Key;
import id.walt.crypto.keys.KeyManager;
import id.walt.crypto.keys.jwk.JWKKey;
import id.walt.crypto.utils.JsonUtils;
import id.walt.oid4vc.data.CredentialFormat;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
@Setter
@Getter
public class IssuerService {

    private JWKKey ISSUER_KEY; //Default key
    private String ISSUER_DID;  //Default did

    public CredentialResponse generateSdJwtCredential(SdJwtBaseIssuanceRequest.SdJwtIssuanceRequest issuanceRequest){
        Key issuerKey;
        String issuerDid;

        if(issuanceRequest.getIssuanceKey() != null){
            issuerKey = KeyManager.INSTANCE.resolveSerializedKey(issuanceRequest.getIssuanceKey());
        } else {
            log.info("Could not find a key in the provided request, service uses the default Issuer Key");
            issuerKey = getISSUER_KEY();
        }

        if(issuanceRequest.getIssuerDid() != null){
            issuerDid = issuanceRequest.getIssuerDid();
        } else {
            log.info("Could not find a did in the provided request, service uses the default Issuer Did");
            issuerDid = getISSUER_DID();
        }

        W3CVC preparedVc  = CredentialUtils.populateAttributes(issuanceRequest.getVc(), Map.of(
                JwtTokenConstants.JWT_PAYLOAD_ISSUER, issuerDid,
                JwtTokenConstants.JWT_PAYLOAD_TOKEN_ID, UUID.randomUUID().toString(),
                JwtTokenConstants.JWT_PAYLOAD_ISSUED_AT, String.valueOf(Instant.now().getEpochSecond()),
                JwtTokenConstants.JWT_PAYLOAD_NOT_BEFORE, String.valueOf(Instant.now().getEpochSecond())
        ));


        W3CVC vc = CredentialUtils.updateSubjectAndIssuerDids(preparedVc, issuanceRequest.getSubjectDid(), issuerDid);

        String issuedVc = vc.signSdJwtBlocking(
                issuerKey,
                issuerDid,
                issuanceRequest.getSubjectDid(),
                issuanceRequest.getSelectiveDisclosure(),
                Map.of(),
                Map.of()
        );

        log.info("Issued SD-JWT: {}", issuedVc);
        return new CredentialResponse(CredentialFormat.sd_jwt_vc.getValue(), issuedVc);
    }


    public CredentialResponse generateJwtCredential(JwtBaseIssuanceRequest.JwtIssuanceRequest issuanceRequest){
        Key issuerKey;
        String issuerDid;

        if(issuanceRequest.getIssuanceKey() != null){
            issuerKey = KeyManager.INSTANCE.resolveSerializedKey(issuanceRequest.getIssuanceKey());
        } else {
            log.info("Could not find a key in the provided request, service uses the default Issuer Key");
            issuerKey = getISSUER_KEY();
        }

        if(issuanceRequest.getIssuerDid() != null){
            issuerDid = issuanceRequest.getIssuerDid();
        } else {
            log.info("Could not find a did in the provided request, service uses the default Issuer Did");
            issuerDid = getISSUER_DID();
        }
        W3CVC preparedVc = CredentialUtils.populateAttributes(issuanceRequest.getVc(), Map.of(
                JwtTokenConstants.JWT_PAYLOAD_ISSUER, issuerDid,
                JwtTokenConstants.JWT_PAYLOAD_TOKEN_ID, UUID.randomUUID().toString(),
                JwtTokenConstants.JWT_PAYLOAD_ISSUED_AT, String.valueOf(Instant.now().getEpochSecond()),
                JwtTokenConstants.JWT_PAYLOAD_NOT_BEFORE, String.valueOf(Instant.now().getEpochSecond())
        ));

        W3CVC vc = CredentialUtils.updateSubjectAndIssuerDids(preparedVc, issuanceRequest.getSubjectDid(), issuerDid);

        String issuedVC = vc.signJwsBlocking(
                issuerKey,
                issuerDid,
                issuerDid,
                issuanceRequest.getSubjectDid(),
                Map.of(JwtTokenConstants.JWT_HEADER_TYPE, "jwt"),
                Map.of(
                        JwtTokenConstants.JWT_PAYLOAD_ISSUER, JsonUtils.INSTANCE.javaToJsonElement(issuerDid),
                        JwtTokenConstants.JWT_PAYLOAD_TOKEN_ID, JsonUtils.INSTANCE.javaToJsonElement(UUID.randomUUID().toString()),
                        JwtTokenConstants.JWT_PAYLOAD_ISSUED_AT, JsonUtils.INSTANCE.javaToJsonElement(String.valueOf(Instant.now().getEpochSecond())),
                        JwtTokenConstants.JWT_PAYLOAD_NOT_BEFORE, JsonUtils.INSTANCE.javaToJsonElement(String.valueOf(Instant.now().getEpochSecond()))
                )
        );

        log.info("Issued VC-JWT: {}", issuedVC);

        return new CredentialResponse(CredentialFormat.jwt_vc.getValue(), issuedVC);
    }
}
