package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.verifier;

import com.apicatalog.jsonld.loader.DocumentLoader;
import com.apicatalog.jsonld.loader.HttpLoader;
import com.danubetech.keyformats.crypto.impl.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import eu.gaiax.sdjwt.issuer.service.converter.KeyConverter;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.contexts.AsterXJsonLDContexts;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto.DidService;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.crypto.KeyService;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.enums.SupportedCryptoSuites;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model.KeyDetails;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request.LdVerificationRequest;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response.LdVerificationResponse;
import foundation.identity.jsonld.ConfigurableDocumentLoader;
import foundation.identity.jsonld.JsonLDException;
import foundation.identity.jsonld.JsonLDObject;
import id.walt.crypto.keys.KeyType;
import info.weboftrust.ldsignatures.LdProof;
import info.weboftrust.ldsignatures.verifier.*;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class LdVerifierService {

    private final ObjectMapper objectMapper;
    private final KeyService keyService;
    private final DidService didService;
    private DocumentLoader documentLoader;

    @PostConstruct
    public void initDocumentLoader(){
        ConfigurableDocumentLoader configurableDocumentLoader = new ConfigurableDocumentLoader(AsterXJsonLDContexts.CONTEXTS);
        configurableDocumentLoader.setHttpLoader(HttpLoader.defaultInstance());
        configurableDocumentLoader.setEnableHttps(true);
        configurableDocumentLoader.setEnableHttp(true);

        documentLoader =   configurableDocumentLoader;

    }

    public LdVerificationResponse verify(LdVerificationRequest verificationRequest){

        boolean overall = true;
        List<LdVerificationResponse.VerificationElement> results = new ArrayList<>();

        //extract proof and all verification methods
        try {
            String signedVc =  objectMapper.writeValueAsString(verificationRequest.getVc());
            JsonLDObject vcTBV = JsonLDObject.fromJson(signedVc);
//            vcTBV.setDocumentLoader(AsterXJsonLDContexts.DOCUMENT_LOADER);
            vcTBV.setDocumentLoader(documentLoader);


            // extract all proof elements
            ObjectNode credentialNode = (ObjectNode) objectMapper.readTree(signedVc);
            ArrayNode arrayNode;
            if(credentialNode.get("proof") instanceof ArrayNode){
                arrayNode = (ArrayNode) credentialNode.get("proof");
            } else {
                arrayNode = objectMapper.createArrayNode();
                arrayNode.add(credentialNode.get("proof"));
            }
//            ArrayNode arrayNode = (ArrayNode) credentialNode.get("proof");
            for(JsonNode objectNode: arrayNode){
                String sigElement = objectMapper.writeValueAsString(objectNode);
                LdProof proof = LdProof.fromJson(sigElement);

                //extract key from verification method

                KeyDetails keyDetails = resolveKey(proof.getVerificationMethod().toString());
                // do verify one by one and combine the results
                LdVerifier ldVerifier = getVerifierForCryptoSuiteAndKeyPair(SupportedCryptoSuites.valueOf(proof.getType()), keyDetails.getKeyType(), keyDetails.getKeyMaterial());
                log.info("Got a verifier for verification request element for {}", proof.getVerificationMethod().toString());
                boolean isValid = ldVerifier.verify(vcTBV, proof);
                log.info("Verified !");
                if( !isValid){
                    overall = false;
                }
                log.info("Verification result for keyId: {} is : {}", keyDetails.getKeyId(), isValid);
                results.add(new LdVerificationResponse.VerificationElement(proof.getVerificationMethod().toString(), isValid));
            }

            return new LdVerificationResponse(overall, results);
        } catch (IOException | GeneralSecurityException | JsonLDException e) {
            log.error("Failed to read the verification request due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }

    private KeyDetails resolveKey(String verificationMethod){
        if(verificationMethod.contains("did")){
            // Get did from verification method
            String did = StringUtils.substringBefore(verificationMethod, "#");
            log.info("Extracted Did: {} from verification method: {}", did, verificationMethod);
            // Retrieve a private key associated to did
            return didService.getKeyDetailsFromDid(did);
        }

        // Retrieving the private key
        String keyId = verificationMethod.substring(verificationMethod.lastIndexOf("/") +1 );
        log.info("Extracting keyId: {}", keyId);

        return keyService.getKeyDetailsById(keyId);
    }

    private LdVerifier getVerifierForCryptoSuiteAndKeyPair(SupportedCryptoSuites cryptoSuites, KeyType keyType, String jwk){
        log.info("Creating a LD Verifier for CryptoSuite: {} and keyType: {}", cryptoSuites, keyType.name());
        return switch (cryptoSuites.getDetails()){
            case "RsaSignature2018" -> new RsaSignature2018LdVerifier(KeyConverter.toRSAPublicKey(jwk));
            case "JsonWebSignature2020" -> {
                if(keyType == KeyType.RSA)
                    yield new JsonWebSignature2020LdVerifier(new RSA_RS256_PublicKeyVerifier(KeyConverter.toRSAPublicKey(jwk)));
                else if (keyType == KeyType.Ed25519)
                    yield new JsonWebSignature2020LdVerifier(new Ed25519_EdDSA_PublicKeyVerifier(KeyConverter.jsonToEd25519PublicKey(jwk)));
                else if (keyType == KeyType.secp256k1){
                    org.bitcoinj.core.ECKey key = KeyConverter.jsonToBitcoinECKey(jwk);
                    yield (new JsonWebSignature2020LdVerifier(new secp256k1_ES256K_PublicKeyVerifier(org.bitcoinj.core.ECKey.fromPublicOnly(key.getPubKey()))));
                }
                else
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("the keyType: %s is not supported yet for JsonWebSignature2020", keyType.name()));
            }
            case "Ed25519Signature2018" -> new Ed25519Signature2018LdVerifier(KeyConverter.jsonToEd25519PublicKey(jwk));
            case "Ed25519Signature2020" -> new Ed25519Signature2020LdVerifier(KeyConverter.jsonToEd25519PublicKey(jwk));
            case "EcdsaSecp256k1Signature2019" -> {
                org.bitcoinj.core.ECKey key = KeyConverter.jsonToBitcoinECKey(jwk);
                yield ( new EcdsaSecp256k1Signature2019LdVerifier(key));
            }
            default -> throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Cryptosuite: %s is not supported", cryptoSuites.getDetails()));
        };
    }
}
