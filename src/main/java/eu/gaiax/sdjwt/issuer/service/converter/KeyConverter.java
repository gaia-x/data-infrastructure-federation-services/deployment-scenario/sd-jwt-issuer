package eu.gaiax.sdjwt.issuer.service.converter;

import com.danubetech.keyformats.crypto.impl.secp256k1_ES256K_PrivateKeySigner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.OctetKeyPair;
import com.nimbusds.jose.jwk.RSAKey;
import id.walt.crypto.keys.KeyType;
import info.weboftrust.ldsignatures.signer.EcdsaSecp256k1Signature2019LdSigner;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.*;
import java.text.ParseException;
import java.util.Base64;


@Slf4j
public class KeyConverter {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static KeyPair jsonToKey(KeyType keyType, String rawJwk){
        log.info("Converting key with details: keyType: {} and jwk: {}", keyType, rawJwk);
        JsonNode keyNode = null;
        String jwk;
        try {
            keyNode = objectMapper.readTree(rawJwk);
            jwk = keyNode.get("jwk").asText();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return switch (keyType){
            case RSA -> jsonToRSAKeyPair(jwk);
            case Ed25519 -> jsonToEd25519KeyPair(jwk);
            case secp256k1, secp256r1 -> jsonToECKeyPair(jwk);
        };
    }

    public static KeyPair jsonToRSAKeyPair(String jwk){
        try {
            RSAKey rsaKey = RSAKey.parse(jwk);

            RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(rsaKey.getModulus().decodeToBigInteger(), rsaKey.getPublicExponent().decodeToBigInteger());

            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            RSAPublicKey publicKey = (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);

            // Create RSA private key
            RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(rsaKey.getModulus().decodeToBigInteger(), rsaKey.getPrivateExponent().decodeToBigInteger());
            RSAPrivateKey privateKey = (RSAPrivateKey) keyFactory.generatePrivate(privateKeySpec);

            return new KeyPair(publicKey, privateKey);
        } catch (ParseException e) {
            log.error("Failed to parse RSA Key due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        } catch (NoSuchProviderException e) {
            log.error("Could not find the specified provider due to: {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }
    public static RSAPublicKey toRSAPublicKey(String jwk){
        try {
            log.info("Converting Jwk: {} to RSA Public Key", jwk);
            JsonNode keyNode = null;
            keyNode = objectMapper.readTree(jwk);
            String keyJwk  = keyNode.get("jwk").asText();

            RSAKey rsaKey = RSAKey.parse(keyJwk);
            RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(rsaKey.getModulus().decodeToBigInteger(), rsaKey.getPublicExponent().decodeToBigInteger());

            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");

            return (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);

        } catch (JsonProcessingException | ParseException | NoSuchAlgorithmException | InvalidKeySpecException |
                 NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
    }

    public static RSAPrivateKey toRSAPrivateKey(String jwk){
        try {
            log.info("Converting Jwk: {} to RSA PrivateKey", jwk);
            JsonNode keyNode = null;
            keyNode = objectMapper.readTree(jwk);
            String keyJwk  = keyNode.get("jwk").asText();
            RSAKey rsaKey = RSAKey.parse(keyJwk);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(rsaKey.getModulus().decodeToBigInteger(), rsaKey.getPrivateExponent().decodeToBigInteger());


            return (RSAPrivateKey)keyFactory.generatePrivate(privateKeySpec);

        } catch (JsonProcessingException | NoSuchAlgorithmException | NoSuchProviderException | ParseException |
                 InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }


    public static KeyPair jsonToEd25519KeyPair(String jwk){
        try {
//            OctetKeyPair octetKeyPair = OctetKeyPair.parse(jwk);
//            EdECPublicKey publicKey = (EdECPublicKey) octetKeyPair.toPublicKey();
//            EdECPrivateKey privateKey = (EdECPrivateKey) octetKeyPair.toPrivateKey();
            OctetKeyPair octetKeyPair = OctetKeyPair.parse(jwk);

//            // get public exponent
//            byte[] x = octetKeyPair.getX().decode();
//            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(x);
//            KeyFactory keyFactory = KeyFactory.getInstance("EdDSA");
//            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
//
//            byte[] d = octetKeyPair.getD().decode();
//            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(d);
//            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            // Convert to KeyPair (Java objects)
            PublicKey publicKey = KeyFactory.getInstance("EdDSA").
                    generatePublic(new X509EncodedKeySpec(octetKeyPair.toPublicKey().getEncoded()));

            PrivateKey privateKey = KeyFactory.getInstance("EdDSA")
                    .generatePrivate(new PKCS8EncodedKeySpec(octetKeyPair.toPrivateKey().getEncoded()));


            return new KeyPair(publicKey, privateKey);
        } catch (ParseException e) {
            log.error("Failed to parse Ed25519 Key due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            log.error("Failed to generate Ed25518 Key due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        }
    }


    public static KeyPair jsonToECKeyPair(String jwk){
        try {
            ECKey ecKey = ECKey.parse(jwk);

            org.bitcoinj.core.ECKey.fromPrivate(ecKey.toPrivateKey().getEncoded());
            return new KeyPair(ecKey.toPublicKey(),ecKey.toPrivateKey());

        } catch (ParseException | JOSEException e){
            log.error("Failed to parse EC Key due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }


    public static org.bitcoinj.core.ECKey jsonToBitcoinECKey(String jwk){
        try {
            log.info("Converting Jwk: {} to Bitcoin ECKey", jwk);
            JsonNode keyNode = objectMapper.readTree(jwk);
            String keyJwk  = keyNode.get("jwk").asText();
            com.nimbusds.jose.jwk.ECKey ecKey = com.nimbusds.jose.jwk.ECKey.parse(keyJwk);

            byte[] privateKeyBytes = Base64.getUrlDecoder().decode(ecKey.getD().toString());
            return org.bitcoinj.core.ECKey.fromPrivate(privateKeyBytes);
        } catch (ParseException | JsonProcessingException e) {
            log.error("Failed to obtain BTC EC Key due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }

    public static byte[] jsonToEd25519PrivateKey(String jwk){
        try {
            log.info("Converting Jwk: {} to Ed25519 Private Key", jwk);
            JsonNode keyNode = objectMapper.readTree(jwk);
            String keyJwk  = keyNode.get("jwk").asText();

            // Parse the JWK
            OctetKeyPair octetKeyPair = OctetKeyPair.parse(keyJwk);
            byte[] privateKey = octetKeyPair.getDecodedX();
            byte[] publicKey = octetKeyPair.getDecodedD();

            byte[] combinedPrivateKey = new byte[64];
            System.arraycopy(publicKey, 0, combinedPrivateKey, 0, 32);
            System.arraycopy(privateKey, 0, combinedPrivateKey, 32, 32);
            return combinedPrivateKey;

        } catch (ParseException | JsonProcessingException e) {
            log.error("Failed to obtain Ed25519 Private Key due to: {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }

    public static byte[] jsonToEd25519PublicKey(String jwk){
        try {
            log.info("Converting Jwk: {} to Ed25519 Public Key", jwk);
            JsonNode keyNode = objectMapper.readTree(jwk);
            String keyJwk  = keyNode.get("jwk").asText();
            // Parse the JWK
            OctetKeyPair octetKeyPair = OctetKeyPair.parse(keyJwk);

            return Base64.getUrlDecoder().decode(octetKeyPair.getX().toString());
        } catch (ParseException | JsonProcessingException e) {
            log.error("Failed to obtain Ed25519 Public Key due to: {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }
}
