package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model;


import id.walt.crypto.keys.KeyType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyImportDetails {
    private KeyType keyType;
    private Map<String, String> details = new HashMap<>();

}
