package eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.request;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import eu.gaiax.sdjwt.issuer.utils.IssuanceUtils;
import id.walt.credentials.vc.vcs.W3CVC;
import id.walt.sdjwt.SDMap;
import jakarta.validation.constraints.NotNull;
import kotlinx.serialization.json.Json;
import kotlinx.serialization.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;


@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SdJwtBaseIssuanceRequest {

    @JsonIgnore
    private final Gson gson = new Gson();

    private Map<String, Object> issuanceKey;

    @JsonIgnore
    private String issuerDid;

    private Map<String, Object> vc;

    @NotNull
    private String subjectDid;

    private SDMapAdapter selectiveDisclosure;


    public SdJwtIssuanceRequest toSdJwtIssuanceRequest(){
        // Convert BaseIssuanceRequest to JwtIssuanceRequest
        SdJwtIssuanceRequest sdJwtIssuanceRequest = new SdJwtIssuanceRequest();

        JsonObject issuanceKeyJson = null;
        if(issuanceKey != null){
            String issuanceKey = gson.toJson(this.getIssuanceKey());
            issuanceKeyJson = (JsonObject) Json.Default.parseToJsonElement(issuanceKey);
        }

        if(! StringUtils.isEmpty(IssuanceUtils.getIssuerFromCredentialTemplate(vc))){
            issuerDid = IssuanceUtils.getIssuerFromCredentialTemplate(vc);
            log.info("Found issuer did: {}", issuerDid);
            if(issuanceKey == null){
                log.error("Request contains a issuer: {} did but do not provide a signing key", issuerDid);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Request contains a issuer: %s did but do not provide a signing key", issuerDid));
            }
        } else {
            log.info("Could not find a pre-configured issuer did");
            issuerDid = null;
        }
        sdJwtIssuanceRequest.setIssuerDid(issuerDid);

        sdJwtIssuanceRequest.setVc(IssuanceUtils.convertToW3CVC(this.getVc()));
        sdJwtIssuanceRequest.setIssuanceKey(issuanceKeyJson);

        sdJwtIssuanceRequest.setSelectiveDisclosure(this.selectiveDisclosure.convertToSDMap());
        sdJwtIssuanceRequest.setSubjectDid(this.subjectDid);
        return sdJwtIssuanceRequest;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SdJwtIssuanceRequest {

        private JsonObject issuanceKey;
        private String issuerDid;
        private W3CVC vc;
        private SDMap selectiveDisclosure;
        private String subjectDid;
    }
}
