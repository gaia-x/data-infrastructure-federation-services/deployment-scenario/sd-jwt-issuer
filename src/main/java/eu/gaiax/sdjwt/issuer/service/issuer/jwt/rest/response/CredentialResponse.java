package eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CredentialResponse {

    private String type;
    private String credential;
}
