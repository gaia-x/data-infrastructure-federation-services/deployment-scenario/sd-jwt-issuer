package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.model;


import id.walt.crypto.keys.KeyType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyDetails {

    private String keyId;
    private KeyType keyType;
    private String keyMaterial;
    private String did;
    private String didDocument;
}
