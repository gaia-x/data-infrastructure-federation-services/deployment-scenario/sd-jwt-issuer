package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LdSignatureResponse {

    private Map<String, Object> vc;
}
