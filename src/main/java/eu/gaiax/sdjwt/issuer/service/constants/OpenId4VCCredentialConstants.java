package eu.gaiax.sdjwt.issuer.service.constants;

public interface OpenId4VCCredentialConstants {

    String W3CDM2_CONTEXT = "@context";
    String W3CDM2_TYPE = "type";
    String W3CDM2_VALID_FORM = "validFrom";
    String W3CDM2_VALID_UNTIL = "validUntil";

    String W3CDM2_ISSUER = "issuer";
    String W3CDM2_ISSUER_ID = "id";
    String W3CDM2_CREDENTIAL_STATUS = "credentialStatus";
    String W3CDM2_CREDENTIAL_SUBJECT = "credentialSubject";
    String W3CDM2_CREDENTIAL_SUBJECT_ID = "id";
}
