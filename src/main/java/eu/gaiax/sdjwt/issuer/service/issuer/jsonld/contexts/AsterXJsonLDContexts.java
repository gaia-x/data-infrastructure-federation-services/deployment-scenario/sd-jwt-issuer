package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.contexts;

import com.apicatalog.jsonld.JsonLdError;
import com.apicatalog.jsonld.document.JsonDocument;
import com.apicatalog.jsonld.http.media.MediaType;
import com.apicatalog.jsonld.loader.DocumentLoader;
import foundation.identity.jsonld.ConfigurableDocumentLoader;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AsterXJsonLDContexts {

    public static final URI JSONLD_CONTEXT_W3ID_SECURITY_V1 = URI.create("https://w3id.org/security/v1");
    public static final URI JSONLD_CONTEXT_W3ID_SECURITY_V2 = URI.create("https://w3id.org/security/v2");
    public static final URI JSONLD_CONTEXT_W3ID_SECURITY_V3 = URI.create("https://w3id.org/security/v3");
    public static final URI JSONLD_CONTEXT_W3ID_SECURITY_BBS_V1 = URI.create("https://w3id.org/security/bbs/v1");
    public static final URI JSONLD_CONTEXT_W3ID_SUITES_SECP256K1_2019_V1 = URI.create("https://w3id.org/security/suites/secp256k1-2019/v1");
    public static final URI JSONLD_CONTEXT_W3ID_SUITES_ED25519_2018_V1 = URI.create("https://w3id.org/security/suites/ed25519-2018/v1");
    public static final URI JSONLD_CONTEXT_W3ID_SUITES_ED25519_2020_V1 = URI.create("https://w3id.org/security/suites/ed25519-2020/v1");
    public static final URI JSONLD_CONTEXT_W3ID_SUITES_X25519_2019_V1 = URI.create("https://w3id.org/security/suites/x25519-2019/v1");
    public static final URI JSONLD_CONTEXT_W3ID_SUITES_JWS_2020_V1 = URI.create("https://w3id.org/security/suites/jws-2020/v1");
    public static final URI JSONLD_CONTEXT_ASTER_X_CREDENTIAL_V1 = URI.create("https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials");
    public static final URI JSONLD_CONTEXT_ASTER_X_SECURITY_V1 = URI.create("https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020");
    public static final URI JSONLD_CONTEXT_CREDENTIAL_DATAMODEL_W3C_V2 = URI.create("https://www.w3.org/ns/credentials/v2");
    public static final URI JSONLD_CONTEXT_CREDENTIAL_DATAMODEL_EXAMPLES_W3C_V2 = URI.create("https://www.w3.org/ns/credentials/examples/v2");
    public static final URI JSONLD_CONTEXT_SECURITY_DATA_INTEGRITY_V2 = URI.create("https://w3id.org/security/data-integrity/v2");
    public static final URI JSONLD_CONTEXT_GAIA_X_TRUST_FRAMEWORK = URI.create("https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#");
    public static final URI JSONLD_CONTEXT_W3C_CREDENTIAL_V1 = URI.create("https://www.w3.org/2018/credentials/v1");

    public static final Map<URI, JsonDocument> CONTEXTS;
    public static final DocumentLoader DOCUMENT_LOADER;


    static {

//        try {
//
//            CONTEXTS = new HashMap<>();
//
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("security-v1.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_V2,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("security-v2.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_V3,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("security-v3-unstable.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_BBS_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("security-bbs-v1.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_SECP256K1_2019_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("suites-secp256k1-2019.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_ED25519_2018_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("suites-ed25519-2018.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_ED25519_2020_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("suites-ed25519-2020.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_X25519_2019_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("suites-x25519-2019.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_JWS_2020_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("suites-jws-2020.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_ASTER_X_CREDENTIAL_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("aster-x-credential.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_ASTER_X_SECURITY_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("aster-x-security.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_CREDENTIAL_DATAMODEL_W3C_V2,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("credential-datamodel-w3c-v2.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_CREDENTIAL_DATAMODEL_EXAMPLES_W3C_V2,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("credential-datamodel-examples-w3c-v2.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_SECURITY_DATA_INTEGRITY_V2,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("security-data-integrity-v2.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_GAIA_X_TRUST_FRAMEWORK,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("gaia-x-trust-framework.jsonld"))));
//            CONTEXTS.put(JSONLD_CONTEXT_W3C_CREDENTIAL_V1,
//                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(AsterXJsonLDContexts.class.getClassLoader().getResourceAsStream("gaia-x-trust-framework.jsonld"))));
//
//
//
//            for (Map.Entry<URI, JsonDocument> context : CONTEXTS.entrySet()) {
//                context.getValue().setDocumentUrl(context.getKey());
//            }
//        } catch (JsonLdError ex) {
//
//            throw new ExceptionInInitializerError(ex);
//        }

        try {

            CONTEXTS = new HashMap<>();

            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("security-v1.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_V2,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("security-v2.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_V3,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("security-v3-unstable.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SECURITY_BBS_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("security-bbs-v1.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_SECP256K1_2019_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("suites-secp256k1-2019.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_ED25519_2018_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("suites-ed25519-2018.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_ED25519_2020_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("suites-ed25519-2020.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_X25519_2019_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("suites-x25519-2019.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3ID_SUITES_JWS_2020_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("suites-jws-2020.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_ASTER_X_CREDENTIAL_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("aster-x-credential.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_ASTER_X_SECURITY_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("aster-x-security.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_CREDENTIAL_DATAMODEL_W3C_V2,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("credential-datamodel-w3c-v2.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_CREDENTIAL_DATAMODEL_EXAMPLES_W3C_V2,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("credential-datamodel-examples-w3c-v2.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_SECURITY_DATA_INTEGRITY_V2,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("security-data-integrity-v2.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_GAIA_X_TRUST_FRAMEWORK,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("gaia-x-trust-framework.jsonld").getInputStream())));
            CONTEXTS.put(JSONLD_CONTEXT_W3C_CREDENTIAL_V1,
                    JsonDocument.of(MediaType.JSON_LD, Objects.requireNonNull(new ClassPathResource("gaia-x-trust-framework.jsonld").getInputStream())));



            for (Map.Entry<URI, JsonDocument> context : CONTEXTS.entrySet()) {
                context.getValue().setDocumentUrl(context.getKey());
            }
        } catch (JsonLdError ex) {

            throw new ExceptionInInitializerError(ex);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        DOCUMENT_LOADER = new ConfigurableDocumentLoader(CONTEXTS);
    }
}
