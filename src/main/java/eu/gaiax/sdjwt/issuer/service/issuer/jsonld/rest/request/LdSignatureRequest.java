package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request;

import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.enums.SupportedCryptoSuites;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class LdSignatureRequest {

    @NotEmpty
    private Map<String, Object> vc;

    @NotEmpty
    private List<Signer> signers;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Signer {
        private String keyId;
        private int signingOrder;
        private SupportedCryptoSuites cryptoSuite;
    }
}
