package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DidRequest {
    private String keyId;
}
