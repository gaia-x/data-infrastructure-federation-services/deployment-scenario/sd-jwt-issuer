package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.rest.response;

import id.walt.crypto.keys.KeyType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyResponse {
    private String keyId;
    private KeyType keyType;
    private String publicKey;
}
