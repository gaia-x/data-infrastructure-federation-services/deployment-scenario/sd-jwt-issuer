package eu.gaiax.sdjwt.issuer.service.issuer.jsonld.enums;


import lombok.Getter;

@Getter
public enum ProofTypes {

    PROOF_SET("PROOF_SET"),
    PROOF_CHAIN("PROOF_CHAIN");
    private final String type;

    ProofTypes(String type){
        this.type = type;
    }
}
