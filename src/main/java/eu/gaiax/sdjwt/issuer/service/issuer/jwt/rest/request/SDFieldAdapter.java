package eu.gaiax.sdjwt.issuer.service.issuer.jwt.rest.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class SDFieldAdapter {

    private Boolean sd;

    private SDMapAdapter children;

}
