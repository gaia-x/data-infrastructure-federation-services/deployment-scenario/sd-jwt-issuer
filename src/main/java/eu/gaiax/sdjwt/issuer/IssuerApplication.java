package eu.gaiax.sdjwt.issuer;

import eu.gaiax.sdjwt.issuer.service.issuer.jwt.IssuerService;
import id.walt.crypto.keys.KeyType;
import id.walt.crypto.keys.jwk.JWKKey;
import id.walt.did.dids.DidService;
import id.walt.did.dids.registrar.dids.DidKeyCreateOptions;
import id.walt.did.helpers.WaltidServices;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@OpenAPIDefinition(
		info = @Info(
				title = "Credential Signing Service",
				summary = "Signing Service for Jwt and Sd-Jwt & JSON-LD Verifiable Credentials",
				version = "1.0",
				contact = @Contact(
						name = "Van Hoan Hoang",
						email = "van-hoan.hoang@eviden.com"
				)
		)
)
@SpringBootApplication
@Slf4j
public class IssuerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(IssuerApplication.class, args);
	}

	@Autowired
	private IssuerService issuerService;

	@Value("${issuer.config.default.did.key.algo}")
	private String keyAlgo;

	@Value("${issuer.config.default.did.method}")
	private String didMethod;

	@Override
	public void run(String... args) {

		WaltidServices.INSTANCE.minimalInitBlocking();

		issuerService.setISSUER_KEY(JWKKey.Companion.generateBlocking(KeyType.valueOf(keyAlgo), null));
		issuerService.setISSUER_DID(DidService.INSTANCE.javaRegisterByKeyBlocking(
				didMethod,
				issuerService.getISSUER_KEY(),
				new DidKeyCreateOptions()).getDid());
		log.info("Successfully initiated default Issuer Key & Issuer DID:  {}", issuerService.getISSUER_DID());

	}
}