package eu.gaiax.sdjwt.issuer;


import com.apicatalog.jsonld.loader.HttpLoader;
import com.danubetech.keyformats.crypto.impl.RSA_RS256_PrivateKeySigner;
import com.danubetech.keyformats.crypto.impl.RSA_RS256_PublicKeyVerifier;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.contexts.AsterXJsonLDContexts;
import eu.gaiax.sdjwt.issuer.utils.KeyUtils;
import foundation.identity.jsonld.ConfigurableDocumentLoader;
import foundation.identity.jsonld.JsonLDException;
import foundation.identity.jsonld.JsonLDObject;
import info.weboftrust.ldsignatures.LdProof;
import info.weboftrust.ldsignatures.jsonld.LDSecurityKeywords;
import info.weboftrust.ldsignatures.signer.Ed25519Signature2018LdSigner;
import info.weboftrust.ldsignatures.signer.JsonWebSignature2020LdSigner;
import info.weboftrust.ldsignatures.signer.LdSigner;
import info.weboftrust.ldsignatures.signer.RsaSignature2018LdSigner;
import info.weboftrust.ldsignatures.verifier.JsonWebSignature2020LdVerifier;
import info.weboftrust.ldsignatures.verifier.LdVerifier;
import info.weboftrust.ldsignatures.verifier.RsaSignature2018LdVerifier;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Slf4j
public class Main {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) throws DecoderException, JsonLDException, GeneralSecurityException, IOException {



//        testIntegritySuite();
//        testSign();

        testJsonWebSignature2020();

    }


    public static void testSign() throws DecoderException, JsonLDException, GeneralSecurityException, IOException {

        String vc  = """
                    {
                       "@context": [
                           "https://www.w3.org/ns/credentials/v2",
                           "https://www.w3.org/ns/credentials/examples/v2"
                         ],
                         "id": "http://example.gov/credentials/3732",
                         "type": ["VerifiableCredential", "ExampleDegreeCredential"],
                         "issuer": "did:example:6fb1f712ebe12c27cc26eebfe11",
                         "validFrom": "2010-01-01T19:23:24Z",
                         "credentialSubject": {
                           "id": "https://subject.example/subject/3921",
                           "degree": {
                             "type": "ExampleBachelorDegree",
                             "name": "Bachelor of Science and Arts"
                           }
                         }
                    }
                """;


        JsonLDObject jsonLdObject = JsonLDObject.fromJson(vc);

//        jsonLdObject.setDocumentLoader(LDSecurityContexts.DOCUMENT_LOADER);
//        jsonLdObject.setDocumentLoader(new SchemeRouter());
        jsonLdObject.setDocumentLoader(AsterXJsonLDContexts.DOCUMENT_LOADER);
        byte[] testEd25519PrivateKey = Hex.decodeHex("984b589e121040156838303f107e13150be4a80fc5088ccba0b0bdc9b1d89090de8777a28f8da1a74e7a13090ed974d879bf692d001cddee16e4cc9f84b60580".toCharArray());

        Ed25519Signature2018LdSigner signer = new Ed25519Signature2018LdSigner(testEd25519PrivateKey);
        signer.setCreated(new Date());
        signer.setProofPurpose(LDSecurityKeywords.JSONLD_TERM_ASSERTIONMETHOD);
        signer.setVerificationMethod(URI.create("https://example.com/jdoe/keys/1"));
//        signer.setDomain("example.com");
        signer.setNonce("343s$FSFDa-");
        LdProof ldProof = signer.sign(jsonLdObject);

        System.out.println(jsonLdObject.toJson(true));


    }

    public static void testIntegritySuite() throws DecoderException, JsonLDException, GeneralSecurityException, IOException {
        String vc = """
                {
                   "@context": [
                           "https://www.w3.org/ns/credentials/v2",
                           "https://www.w3.org/ns/credentials/examples/v2"
                         ],
                  "myWebsite": "https://hello.world.example/",
                  "proof": {
                    "type": "DataIntegrityProof",
                    "cryptosuite": "eddsa-jcs-2022",
                    "created": "2023-03-05T19:23:24Z",
                    "verificationMethod": "https://di.example/issuer#z6MkjLrk3gKS2nnkeWcmcxiZPGskmesDpuwRBorgHxUXfxnG",
                    "proofPurpose": "assertionMethod",
                    "proofValue": "zQeVbY4oey5q2M3XKaxup3tmzN4DRFTLVqpLMweBrSxMY2xHX5XTYV8nQApmEcqaqA3Q1gVHMrXFkXJeV6doDwLWx"
                  }
                }
                """;
        String vc1 = """
                    {
                       "@context":[
                          "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
                          "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
                       ],
                       "@type":[
                          "VerifiableCredential"
                       ],
                       "@id":"https://ovhcloud.provider.dev.gxdch.ovh/legal-participant/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
                       "issuer":"did:web:ovhcloud.provider.dev.gxdch.ovh",
                       "credentialSubject":{
                          "id":"https://ovhcloud.provider.dev.gxdch.ovh/legal-participant-json/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
                          "gx:legalName":"OVHCLOUD",
                          "gx:legalRegistrationNumber":{
                             "id":"https://ovhcloud.provider.dev.gxdch.ovh/gaiax-legal-registration-number/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json"
                          },
                          "gx:headquarterAddress":{
                             "gx:countrySubdivisionCode":"FR-59"
                          },
                          "gx:legalAddress":{
                             "gx:countrySubdivisionCode":"FR-59"
                          },
                          "@context":[
                             "https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
                          ],
                          "type":"gx:LegalParticipant"
                       },
                       "expirationDate":"2024-11-12T15:53:43.145623+00:00",
                       "issuanceDate":"2024-05-16T15:53:43.145623+00:00",
                       "credentialStatus":{
                          "type":"StatusList2021Entry",
                          "id":"https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/ovhcloud-revocation#3588",
                          "statusPurpose":"revocation",
                          "statusListIndex":"3588",
                          "statusListCredential":"https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/ovhcloud-revocation"
                       }
                    }
                """;

        JsonLDObject jsonLdObject = JsonLDObject.fromJson(vc1);

        jsonLdObject.setDocumentLoader(AsterXJsonLDContexts.DOCUMENT_LOADER);
        byte[] testEd25519PrivateKey = Hex.decodeHex("984b589e121040156838303f107e13150be4a80fc5088ccba0b0bdc9b1d89090de8777a28f8da1a74e7a13090ed974d879bf692d001cddee16e4cc9f84b60580".toCharArray());

//        Ed25519Signature2018LdSigner signer = new Ed25519Signature2018LdSigner();
        JsonWebSignature2020LdSigner jsonWebSignature2020LdSigner = new JsonWebSignature2020LdSigner();

        RsaSignature2018LdSigner signer = new RsaSignature2018LdSigner(KeyUtils.testRSAPrivateKey);

        signer.setCreated(new Date());
        signer.setProofPurpose(LDSecurityKeywords.JSONLD_TERM_ASSERTIONMETHOD);
        signer.setVerificationMethod(URI.create("https://example.com/jdoe/keys/1"));
        signer.setNonce("343s$FSFDa-");
        LdProof ldProof = signer.sign(jsonLdObject);

        System.out.println(jsonLdObject.toJson(true));
        System.out.println("Proof: "+ ldProof.toJson());


//        Ed25519Signature2018LdVerifier verifier = new Ed25519Signature2018LdVerifier()
        RsaSignature2018LdVerifier verifier = new RsaSignature2018LdVerifier(KeyUtils.testRSAPublicKey);
        boolean verify = verifier.verify(jsonLdObject, ldProof);

        System.out.println("Verify: "+verify);
    }




    public static void testJsonWebSignature2020() throws JsonLDException, GeneralSecurityException, IOException {
        String vc = """
                {
                   "@context": [
                           "https://www.w3.org/ns/credentials/v2",
                           "https://www.w3.org/ns/credentials/examples/v2"
                         ],
                  "myWebsite": "https://hello.world.example/"
                
                }
                """;
        JsonLDObject jsonLdObject = JsonLDObject.fromJson(vc);
//        jsonLdObject.setDocumentLoader(AsterXJsonLDContexts.DOCUMENT_LOADER);;

        ConfigurableDocumentLoader documentLoader = new ConfigurableDocumentLoader();
        documentLoader.setHttpLoader(HttpLoader.defaultInstance());
        documentLoader.setEnableHttp(true);
        documentLoader.setEnableHttps(true);

        jsonLdObject.setDocumentLoader(documentLoader);

        LdSigner signer = new JsonWebSignature2020LdSigner(new RSA_RS256_PrivateKeySigner(KeyUtils.testRSAPrivateKey));

        signer.setCreated(new Date());
        signer.setProofPurpose(LDSecurityKeywords.JSONLD_TERM_ASSERTIONMETHOD);
        signer.setVerificationMethod(URI.create("https://example.com/jdoe/keys/1"));
        signer.setNonce("343s$FSFDa-");
        LdProof ldProof = signer.sign(jsonLdObject);

        System.out.println(jsonLdObject.toJson(true));
        System.out.println("Proof: "+ ldProof.toJson());


        LdVerifier verifier = new JsonWebSignature2020LdVerifier(new RSA_RS256_PublicKeyVerifier(KeyUtils.testRSAPublicKey));

//        Ed25519Signature2018LdVerifier verifier = new Ed25519Signature2018LdVerifier()
//        RsaSignature2018LdVerifier verifier = new RsaSignature2018LdVerifier(KeyUtils.testRSAPublicKey);
        boolean verify = verifier.verify(jsonLdObject, ldProof);

        System.out.println("Verify: "+verify);


    }


}
