package eu.gaiax.sdjwt.issuer.config;

import eu.gaiax.sdjwt.issuer.service.issuer.jsonld.enums.SupportedCryptoSuites;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.convert.converter.Converter;

public class CryptoSuiteConverter implements Converter<String, SupportedCryptoSuites> {

    @Override
    public SupportedCryptoSuites convert(@NotNull String source) {
        return SupportedCryptoSuites.valueOf(source);
    }
}
