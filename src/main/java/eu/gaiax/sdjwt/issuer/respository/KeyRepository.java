package eu.gaiax.sdjwt.issuer.respository;

import eu.gaiax.sdjwt.issuer.entity.KeyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeyRepository extends JpaRepository<KeyEntity, Long> {

}
