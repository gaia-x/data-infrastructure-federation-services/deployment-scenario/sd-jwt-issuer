package eu.gaiax.sdjwt.issuer.respository;

import eu.gaiax.sdjwt.issuer.entity.DidEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DidRepository extends JpaRepository<DidEntity, Long> {

    Optional<DidEntity> findByDid(String did);
    boolean existsByDidPath(String didPath);
    Optional<DidEntity> findByDidPath(String didPath);
}
