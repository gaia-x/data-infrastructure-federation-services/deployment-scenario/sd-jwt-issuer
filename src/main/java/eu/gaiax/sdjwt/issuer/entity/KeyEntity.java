package eu.gaiax.sdjwt.issuer.entity;


import id.walt.crypto.keys.KeyType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(
        name = "ccmc_keys"
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KeyEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -4600848146162763514L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "key_type")
    @Enumerated(EnumType.STRING)
    private KeyType keyType;

    @Column(name = "jwk", columnDefinition = "text")
    private String jwk;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @OneToOne(
            mappedBy = "keyEntity",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private DidEntity didEntity;
}
