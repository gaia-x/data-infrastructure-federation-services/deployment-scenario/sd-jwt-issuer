package eu.gaiax.sdjwt.issuer.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DidEntity implements Serializable {


    @Serial
    private static final long serialVersionUID = -1675593167618822921L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "did")
    private String did;

    @Column(name = "did_path", nullable = false)
    private String didPath;

    @Column(name = "did_document", columnDefinition = "text")
    private String didDocument;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @OneToOne
    @JoinColumn(name = "key_id", referencedColumnName = "id")
    private KeyEntity keyEntity;
}
