package eu.gaiax.sdjwt.issuer.data;

public class SampleVCData {

    public static String sdEmployeeVC = """
            {
                  "issuance_key": {
                    "type": "local",
                    "jwk": "{\\"kty\\":\\"OKP\\",\\"d\\":\\"mDhpwaH6JYSrD2Bq7Cs-pzmsjlLj4EOhxyI-9DM1mFI\\",\\"crv\\":\\"Ed25519\\",\\"kid\\":\\"Vzx7l5fh56F3Pf9aR3DECU5BwfrY6ZJe05aiWYWzan8\\",\\"x\\":\\"T3T4-u1Xz3vAV2JwPNxWfs4pik_JLiArz_WTCvrCFUM\\"}"
                  },
                  "issuer_did": "did:key:z6MkjoRhq1jSNJdLiruSXrFFxagqrztZaXHqHGUTKJbcNywp", // OPTIONAL
                  "vc": {
                    "@context": [
                      "https://www.w3.org/2018/credentials/v1"
                    ],
                    "id": "urn:uuid:123",
                    "type": [
                      "VerifiableCredential",
                      "EmployeeCredential"
                    ],
                   
                    "issuer": {
                      "id": "did:key:temporary",
                      "name": "Eviden Issuer"
                    },
                    "issuanceDate": "2024-01-19T07:05:44Z",
                    "expirationDate": "2024-01-19T07:05:44Z",
                    "credentialSubject": {
                      "id": "did:key:123",
                      "firstName": "Van Hoan",
                      "lastName":"Hoang",
                      "team":"Innovation"
                    }
                  },
                  "subject_did":"did:key:z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2#z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2",
                  "mapping": {
                "id": "\u003cuuid\u003e",
                "issuer": {
                  "id": "\u003cissuerDid\u003e"
                },
                "credentialSubject": {
                  "id": "\u003csubjectDid\u003e"
                },
                "issuanceDate": "\u003ctimestamp\u003e",
                "expirationDate": "\u003ctimestamp-in:365d\u003e"
              },
              "selectiveDisclosure":{
                      "fields":{
                         "credentialSubject":{
                            "sd":false,
                            "children":{
                               "decoys":2,
                               "decoyMode":"FIXED",
                               "fields":{
                                  "team":{
                                     "sd":true
                                  }
                               }
                            }
                         }
                      }
                   }
                }
            """;

    public static String employeeVC = """
            {
               "issuance_key":{
                  "type":"local",
                  "jwk":"{\\"kty\\":\\"OKP\\",\\"d\\":\\"mDhpwaH6JYSrD2Bq7Cs-pzmsjlLj4EOhxyI-9DM1mFI\\",\\"crv\\":\\"Ed25519\\",\\"kid\\":\\"Vzx7l5fh56F3Pf9aR3DECU5BwfrY6ZJe05aiWYWzan8\\",\\"x\\":\\"T3T4-u1Xz3vAV2JwPNxWfs4pik_JLiArz_WTCvrCFUM\\"}"
               },
               "issuer_did":"did:key:z6MkjoRhq1jSNJdLiruSXrFFxagqrztZaXHqHGUTKJbcNywp",
               "vc":{
                  "@context":[
                     "https://www.w3.org/2018/credentials/v1"
                  ],
                  "id":"urn:uuid:123",
                  "type":[
                     "VerifiableCredential",
                     "EmployeeCredential"
                  ],
                  "issuer":{
                     "id":"did:key:temporary",
                     "name":"Eviden Issuer"
                  },
                  "issuanceDate":"2024-01-19T07:05:44Z",
                  "expirationDate":"2024-01-19T07:05:44Z",
                  "credentialSubject":{
                     "id":"did:key:123",
                     "firstName":"Van Hoan",
                     "lastName":"Hoang",
                     "team":"Innovation"
                  }
               },
               "subject_did":"did:key:z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2#z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2",
               "mapping":{
                  "id":"<uuid>",
                  "issuer":{
                     "id":"<issuerDid>"
                  },
                  "credentialSubject":{
                     "id":"<subjectDid>"
                  },
                  "issuanceDate":"<timestamp>",
                  "expirationDate":"<timestamp-in:365d>"
               }
            }
    """;

}
