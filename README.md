## Sd-JWT, JWT & Multi-signer Credential Issuing Service
The project is composed of 2 modules and aims at experimenting following concepts:
- Credential signing for SD-JWT & JWT  
- Credential signing for JSON-LD with proofSet to enable multi signers scenarios
## Deploy & Testing

To run the application locally, please make use of the provided docker-compose file and the associated .env file which can be adjusted according to you need.

```
docker compose up --build
```
The command takes care of compiling the source code and spin up the credential signing service as well as postgres container for storage purpose.


## Jwt and Sd-Jwt Credential Signing
The service enables sd-jwt and jwt credentials

For detailed instructions on the module, please refer to [JWT & SD-JWT specification](/documentation/Jwt.md)

## Multi-signer Json-Ld Credential Signing

The service enables credential signing in the context of multi-signers, i.e., contract signing. For this purpose, the service offers the following features:

- Support key generation for 3 different key types, including *RSA, Ed25519, secp256k1* and 5 different crypto suites
- Support key importation
- Support did:web creation
- Support Json-Ld signing and verification


For detailed instructions on the module, please refer to [JSON-LD specification](/documentation/Json-ld.md)
