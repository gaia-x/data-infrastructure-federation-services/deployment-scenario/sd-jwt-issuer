## Jwt and Sd-jwt credential signing
By default, the jwt and sd-jwt credential service is exposed with two endpoints for issuing verifiable credentials in both JWT and SD-JWT formats. You can use the following payload as example to get your first SD-JWT and JWT credentials.

In each payload, in case that you provide a DID for Issuer, you also need to provide a jwk of the Issuer key that you have used to generate the did of the Issuer. Otherwise, the service will generate a default key pair and a did whose the method is configurable via environment variable.

For SD-JWT example:
```
POST /issue/sd-jwt
Header Content-Type: application/json
```
```json
{
  "issuanceKey":{
    "type":"jwk",
    "jwk":"{\"kty\":\"OKP\",\"d\":\"mDhpwaH6JYSrD2Bq7Cs-pzmsjlLj4EOhxyI-9DM1mFI\",\"crv\":\"Ed25519\",\"kid\":\"Vzx7l5fh56F3Pf9aR3DECU5BwfrY6ZJe05aiWYWzan8\",\"x\":\"T3T4-u1Xz3vAV2JwPNxWfs4pik_JLiArz_WTCvrCFUM\"}"
  },
  "vc":{
    "@context":[
      "https://www.w3.org/2018/credentials/v1"
    ],
    "type":[
      "VerifiableCredential",
      "EmployeeCredential"
    ],
    "issuer":{
      "id":"did:web:eviden.com",
      "name":"Eviden Issuer"
    },
    "credentialSubject":{
      "id":"did:key:123",
      "firstName":"Van Hoan",
      "lastName":"Hoang",
      "team":"Innovation"
    }
  },
  "subjectDid":"did:key:z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2#z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2",
  "selectiveDisclosure":{
    "fields":{
      "credentialSubject":{
        "sd":false,
        "children":{
          "decoys":2,
          "decoyMode":"FIXED",
          "fields":{
            "team":{
              "sd":true
            }
          }
        }
      }
    }
  }
}
```


For JWT example:
```
POST /issue/jwt
Header Content-Type: application/json
```
```json
{
  "issuanceKey":{
    "type":"jwk",
    "jwk":"{\"kty\":\"OKP\",\"d\":\"mDhpwaH6JYSrD2Bq7Cs-pzmsjlLj4EOhxyI-9DM1mFI\",\"crv\":\"Ed25519\",\"kid\":\"Vzx7l5fh56F3Pf9aR3DECU5BwfrY6ZJe05aiWYWzan8\",\"x\":\"T3T4-u1Xz3vAV2JwPNxWfs4pik_JLiArz_WTCvrCFUM\"}"
  },
  "vc":{
    "@context":[
      "https://www.w3.org/2018/credentials/v1"
    ],
    "type":[
      "VerifiableCredential",
      "EmployeeCredential"
    ],
    "issuer":{
      "id":"did:web:eviden.com",
      "name":"Eviden Issuer"
    },
    "credentialSubject":{
      "id":"did:key:123",
      "firstName":"Van Hoan",
      "lastName":"Hoang",
      "team":"Innovation"
    }
  },
  "subjectDid":"did:key:z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2#z6Mktcw1ZtZv1Cscc8APxUXaase7zaf4zXtmSQhvFWLTwPu2"
}
```