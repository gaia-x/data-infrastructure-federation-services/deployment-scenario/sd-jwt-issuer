
## Multi-signer Json-Ld Credential Signing
The multi-signer json-ld credential signing offers a rich set of features, ranging from cryptographic key, did creation to credential signing.

### Cryptographic Key Management
LD Signing Service allows creating cryptographic keys with specified metadata, which are then used to generate DID as well as sign credentials. Nonetheless, if needed, you can also import your own key pairs into the system.
The system supports 3 types of cryptographic keys, including *RSA, Ed25519, secp256k1* and 5 different cryptoSuites as detailed below:
- RsaSignature2018 supports only RSA
- JsonWebSignature2020 supports all three key types: RSA, Ed25519, secp256k1
- Ed25519Signature2020 supports Ed25519
- Ed25519Signature2018 supports Ed25519
- EcdsaSecp256k1Signature2019 supports secp256k1

The following requests illustrate how to create a key pair based on given metadata:

In case of RSA, you would need to specify the *keySize* parameter which is at least 2048 bits
```
POST /crypto/keys
Header Content-Type: application/json

{
  "keyType": "RSA",
  "metadata": {
    "keySize": "2048"
  }
}
```

For other key types, simply indicate the type you want to generate:
```json
{
  "keyType": "secp256k1"
}
```
Once the request is submitted, you will get a keyId and publicKey in the response body. An example of such response is below:

```json
{
  "keyId": "202",
  "keyType": "secp256k1",
  "publicKey": "{\"kty\":\"EC\",\"crv\":\"secp256k1\",\"kid\":\"lpHMM9NIv-OqREvdgbbEpNsf7E6yskvApJ2fop7slSk\",\"x\":\"5WDKUS8mUFlVrxg02lcutyW90Df1Bd1VfOOC5-uKFIw\",\"y\":\"lLqY5mn_hfvIr5toesgpyAspCUzwxe4JKAqE2eVAaH8\"}"
}
```

You can also import your keys into the service by the following endpoint:

````
POST /crypto/keys/import
Header Content-Type: application/json

{
  "keyType": "Ed25519",
  "jwk":"{\"kty\":\"OKP\",\"d\":\"mDhpwaH6JYSrD2Bq7Cs-pzmsjlLj4EOhxyI-9DM1mFI\",\"crv\":\"Ed25519\",\"kid\":\"Vzx7l5fh56F3Pf9aR3DECU5BwfrY6ZJe05aiWYWzan8\",\"x\":\"T3T4-u1Xz3vAV2JwPNxWfs4pik_JLiArz_WTCvrCFUM\"}"
}
````
### Json-LD Credential Signing with KeyID
In order to signing a json-ld credential, you must provide an unsigned credential as well as keys to be used and the signing order. One of such example is shown below:

```
POST /credentials/sign
Header Content-Type: application/json

{
  "vc":{
    "@context":[
      "https://www.w3.org/ns/credentials/v2",
      "https://www.w3.org/ns/credentials/examples/v2"
    ],
    "myWebsite":"https://hello.world.example/"
  },
  "signers":[
    {
      "keyId":"1",
      "singingOrder":1,
      "cryptoSuites":"JsonWebSignature2020 "
    },
    {
      "keyId":"9",
      "singingOrder":2,
      "cryptoSuites":"JsonWebSignature2020 "
    }
  ]
}
```
The above signing request contains all key IDs to be used, the signing order as well as the crypto suite. Refer to section above to know the key types supported by each cryptoSuites. Once the signing request is successfully fulfilled, the response will contain a payload similar to the below:  


Response
````json
{
  "vc": {
    "@context": [
      "https://www.w3.org/ns/credentials/v2",
      "https://www.w3.org/ns/credentials/examples/v2",
      "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "myWebsite": "https://hello.world.example/",
    "issuanceDate": "2024-06-24T09:26:56Z",
    "expirationDate": "2024-07-24T09:26:56Z",
    "proof": [
      {
        "type": "JsonWebSignature2020",
        "created": "2024-06-24T09:26:56Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "jwt-issuer.aster-x.demo23.gxfs.fr/crypto/keys/1",
        "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFZERTQSJ9..JW-EMj2Mj0VyfWp--vBUQ2rir-PPZwKpuL9V7BFs77zxJygDmdXQ27vEeHjiRVZCWvA8hC82pFc7WbEfYm4pDA"
      },
      {
        "type": "JsonWebSignature2020",
        "created": "2024-06-24T09:26:57Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "jwt-issuer.aster-x.demo23.gxfs.fr/crypto/keys/9",
        "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJSUzI1NiJ9..SDr6oSQh9FDgIj6ylYp4uoqaFFi_f6qF9hp7S_4zByNt5WATgWjimppROFFMbx5ZGblVFKaSnyr3GW_ezlr5mrc3q6yQksthVgIV8QuH99A6zra76MNY6dFFL9Kez6SVj23WI_X7y2TSySLXlDLWjC1Ja1N9TykwLymG2Parl4BlCCcwyjWZvN7Ee2FB20JUqK-07jUor3bCWpzSfZl0s46-XByqgZIMU_IsRcc8iHwL-IX0NhHH72bS-bpluUKCNOUmv9MbSDEOWznCeqM5vF27uXGdETpbFfRRtP-StK9nImToV0wQ-HhoJlNeAooL5Ny1c62tzIjGktUaDgil-w"
      }
    ]
  }
}
````
The *proof* field is actually an array of proof element. Each proof element contains the metadata provided in the signing request earlier. Closely looking into it, the verificationMethod is currently an URI pointing to the details of the verification key. Resolving this URI return the associated public key.

### Json-LD Credential Signing with DID
To be aligned with SSI concept, the service also provides configuration to replace keyID URI in the verificationMethod by a DID. 
For this purpose, we need to create did:web from previously created keys by as follows:

1. Create DID for a given Key ID

The following request allows did creation with given keyID:

```
POST /crypto/dids
Header: Content-Type: application/json

{
  "keyId": "1"
}
```
Once submitted, a did:web is generated and resolvable. the format of the did is: 
```
did:web:{serverUrl}:{keyId}
```
Given that the service is deployed at *https://jwt-issuer.aster-x.demo23.gxfs.fr/*, the following response should be returned:

````json
{
  "id": "1",
  "did": "did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1",
  "didDocument": "{\"@context\":[\"https://www.w3.org/ns/did/v1\",\"https://w3id.org/security/suites/jws-2020/v1\"],\"id\":\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1\",\"verificationMethod\":[{\"id\":\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1#eMvrZoj03o7KieGMA0SdF4RgE3fwNDw6-Jhz3RxSayE\",\"type\":\"JsonWebKey2020\",\"controller\":\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1\",\"publicKeyJwk\":{\"kty\":\"OKP\",\"crv\":\"Ed25519\",\"kid\":\"eMvrZoj03o7KieGMA0SdF4RgE3fwNDw6-Jhz3RxSayE\",\"x\":\"Xb2twpb7dFZti3pGJ-n8wNgxLHNFTJDUFd_XVzYHe3M\"}}],\"assertionMethod\":[\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1#eMvrZoj03o7KieGMA0SdF4RgE3fwNDw6-Jhz3RxSayE\"],\"authentication\":[\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1#eMvrZoj03o7KieGMA0SdF4RgE3fwNDw6-Jhz3RxSayE\"],\"capabilityInvocation\":[\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1#eMvrZoj03o7KieGMA0SdF4RgE3fwNDw6-Jhz3RxSayE\"],\"capabilityDelegation\":[\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1#eMvrZoj03o7KieGMA0SdF4RgE3fwNDw6-Jhz3RxSayE\"],\"keyAgreement\":[\"did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1#eMvrZoj03o7KieGMA0SdF4RgE3fwNDw6-Jhz3RxSayE\"]}"
}
````

Resolving the *did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1* will return the did document as well. 

2. Credential Signing

Now, using the SAME signing request as above, the system can automatically detect whether a did has been generated for a given keyId, and use it for proof configuration. Below is an example of a signing response: 
```
{
  "vc": {
    "@context": [
      "https://www.w3.org/ns/credentials/v2",
      "https://www.w3.org/ns/credentials/examples/v2",
      "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "myWebsite": "https://hello.world.example/",
    "issuanceDate": "2024-06-24T09:52:27Z",
    "expirationDate": "2024-07-24T09:52:27Z",
    "proof": [
      {
        "type": "JsonWebSignature2020",
        "created": "2024-06-24T09:52:27Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1",
        "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFZERTQSJ9..sPbroj863eY8VZOOV5sgUwuC1dijcd2hGnxtpDkpXNbu0DdxJjowH_i9oKz1ZIBUFnmzW1TZfG_WrqGwWSqYCw"
      },
      {
        "type": "JsonWebSignature2020",
        "created": "2024-06-24T09:52:27Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:jwt-issuer.aster-x.demo23.gxfs.fr:9",
        "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJSUzI1NiJ9..iVl75vIB-lmz8IMB3hwol0ah23V9q6y6rT0h6Yx5kuDcn_USRQHurgWT3smRPTBYMgJaoz_ys8TKQbfDVU5JkmdwsT7WepTopRZOIAZlx6VQ488KXTXTE9HKnD9QoO2RrR__Ztd373l7roenino65I-1l48LLkZe_cY4P--nmcxS4JeoAri7mW7N9Hi7iusuc7cJSDakFODWWnWZeP1cdVGOcH7aSaNkMdnvPYi3HVIbJ2DKp-_wTtjZPDlkwtZEdaY7zKy1bsrMFn7toQS7SUHMp4r54nfqW4kGEoFvsJJEdej1luSRw3IHuZDqIFqGkPwhZlYe2NS8K3h4G7l2JA"
      }
    ]
  }
}
```

### Json-Ld Credential Verification
In order to verify the signatures apposed on a credential, please follow the example below:

````
POST /credentials/verify
Header Content-Type: application/json

{
  "vc": {
    "@context": [
      "https://www.w3.org/ns/credentials/v2",
      "https://www.w3.org/ns/credentials/examples/v2",
      "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "myWebsite": "https://hello.world.example/",
    "issuanceDate": "2024-06-24T09:52:27Z",
    "expirationDate": "2024-07-24T09:52:27Z",
    "proof": [
      {
        "type": "JsonWebSignature2020",
        "created": "2024-06-24T09:52:27Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1",
        "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFZERTQSJ9..sPbroj863eY8VZOOV5sgUwuC1dijcd2hGnxtpDkpXNbu0DdxJjowH_i9oKz1ZIBUFnmzW1TZfG_WrqGwWSqYCw"
      },
      {
        "type": "JsonWebSignature2020",
        "created": "2024-06-24T09:52:27Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:jwt-issuer.aster-x.demo23.gxfs.fr:9",
        "jws": "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJSUzI1NiJ9..iVl75vIB-lmz8IMB3hwol0ah23V9q6y6rT0h6Yx5kuDcn_USRQHurgWT3smRPTBYMgJaoz_ys8TKQbfDVU5JkmdwsT7WepTopRZOIAZlx6VQ488KXTXTE9HKnD9QoO2RrR__Ztd373l7roenino65I-1l48LLkZe_cY4P--nmcxS4JeoAri7mW7N9Hi7iusuc7cJSDakFODWWnWZeP1cdVGOcH7aSaNkMdnvPYi3HVIbJ2DKp-_wTtjZPDlkwtZEdaY7zKy1bsrMFn7toQS7SUHMp4r54nfqW4kGEoFvsJJEdej1luSRw3IHuZDqIFqGkPwhZlYe2NS8K3h4G7l2JA"
      }
    ]
  }
}
````
In return, a payload will have the following format, indicating the validity of each proof element:

```json
{
  "overall": true,
  "verificationElements": [
    {
      "keyId": "did:web:jwt-issuer.aster-x.demo23.gxfs.fr:1",
      "result": true
    },
    {
      "keyId": "did:web:jwt-issuer.aster-x.demo23.gxfs.fr:9",
      "result": true
    }
  ]
}
```