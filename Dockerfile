FROM eclipse-temurin:17-jdk-alpine as build
#FROM maven:3.8.5-openjdk-17 as build

WORKDIR /opt/app

#RUN apk update && apk add --no-cache libsodium

#COPY pom.xml .
#COPY settings.xml /usr/share/maven/ref/
#COPY ./src ./src
COPY ./src ./src
COPY .mvn/ .mvn
COPY pom.xml mvnw ./
RUN chmod +x ./mvnw
RUN ./mvnw clean install


FROM eclipse-temurin:17-jdk-alpine
#FROM maven:3.8.5-openjdk-17
WORKDIR /app

# Install libsodium in the runtime environment
#RUN apk update && apk add --no-cache libsodium

COPY --from=build /opt/app/target/jwt-issuer-v0.1.jar /app/jwt-issuer.jar
ENTRYPOINT ["java", "-jar", "jwt-issuer.jar"]
